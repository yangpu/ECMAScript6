/**
 * apply方法拦截函数的调用、call和apply操作。
 *  apply方法可以接受三个参数，
 * 分别是目标对象、目标对象的上下文对象（this）和目标对象的参数数组。
 * 
 * 
 * 
 * var handler = {
    apply (target, ctx, args) {
      return Reflect.apply(...arguments);
    }
  };
 */


 /**
  * 1.
  * 变量p是 Proxy 的实例，当它作为函数调用时（p()），
  * 就会被apply方法拦截，返回一个字符串
  */
 let target = () => 'I am the target'
 let handler = {
   apply: () => 'I am proxy' 
 }

 let p = new Proxy(target, handler)
//  let str = p()
//  console.log(str) // // "I am the proxy"


/**
 * 2
 */
let twice = {
  apply (target, ctx, args) {
    console.log(target) // 目标对象, 函数
    console.log(ctx) // 上下文对象,undefined/null
    console.log(args) // 目标对象的参数数组,  [1, 2]
    return Reflect.apply(...arguments) * 2
  }
}

function sum (a, b) {
  return a + b
}

let proxy2 = new Proxy(sum, twice)
// console.log(proxy2(1,2)) // 6
// console.log(proxy2.call(null, 1,2)) // 6
// console.log(proxy2.apply(null, [1,2]))// 6

/**
 * 直接调用Reflect.apply方法，也会被拦截。
 */
// console.log(Reflect.apply(proxy2, null, [9, 10])) // 38


let appDom = document.body.appendChild
let appProxy = new Proxy(appDom, {
  apply (target, ctx, args) {
    console.log(args)
    let el = document.createElement('div')
    el.innerHTML = '33333333'
    el.appendChild(...args)
    return Reflect.apply(target, ctx, [el])
  }
})

let el = document.createElement('div')
el.innerHTML = '222222'

appProxy.call(document.body, el)