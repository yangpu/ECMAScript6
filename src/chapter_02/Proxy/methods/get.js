// get方法用于拦截某个属性的读取操作，可以接受三个参数，
// 依次为目标对象、属性名和 proxy 实例本身（严格地说，是操作行为所针对的对象），
// 其中最后一个参数可选。

/**
 * 1
 * 如果访问目标对象不存在的属性，会抛出一个错误。
* 如果没有这个拦截函数，访问不存在的属性，只会返回undefined。
 */

let target = {
  name: '张三'
};
let handler = {
  /**
   * 
   * @param {*} target 
   * @param {*} key 
   */
  get (target, key) {
    if (key in target) {
      return target[key]
    } else {
      throw new ReferenceError(`${key}属性没有定义`)
    }
  }
}

let proxy = new Proxy(target, handler)

// console.log(proxy.name)
// console.log(proxy.foo)




/**
 * 2
 * get方法可以继承
 * 拦截操作定义在Prototype对象上面，
 * 所以如果读取obj对象继承的属性时，拦截会生效。
 */

let proxy2 = new Proxy({}, {
  get (target, key) {
    console.log(`get ${key}`)
    return target[key]
  }
})

// let obj2 = Object.create(proxy2)
// console.log(obj2.foo)



/**
 * 3.使用get拦截，实现数组读取负数的索引。
 * 数组的位置参数是-1，就会输出数组的倒数第一个成员
 */
function createArray (...arr) {
  let handler = {
    get (target, key, recever) {
      console.log(target, key, recever)
      let index = Number(key)
      if (index < 0) {
        key = String(target.length + index)
      }
      return Reflect.get(target, key, recever)
    }
  }


  let target = []
  target.push(...arr)
  return new Proxy(target, handler)
}

let proxy3 = createArray('a', 'b', 'c')
// console.log(proxy3[-1])


/**
 * 4
 * 可以将读取属性的操作（get），转变为执行某个函数，从而实现属性的链式操作。
 */
let double = n => n * 2; // 2倍函数
let pow    = n => n * n; // 平方函数
let reverseInt = n => n.toString().split("").reverse().join("") | 0; // 转换函数

let proxy4 = {
  double,
  pow,
  reverseInt
}

 let pipe = (function(win) { // 闭包,实际也是一个函数
  return function (value) {
    let funStack = []
    let proxy = new Proxy({}, {
      get (target, key, recever) {
        if (key === 'get') {
          // 这里是链式操作的最终目标
          // val 必需。初始值, 或者计算结束后的返回值。
          // fn 当前元素
          return funStack.reduce((val, fn) => fn(val), value)
        }

        funStack.push(win[key]); // 添加函数
        return recever; // 返回拦截器
      }
      
    })

    return proxy; // 返回拦截器
  }
 }(proxy4));



// 首先pip(3)的时候得到一个，初始值为3的拦截器
// 每次链式操作之后，都会把函数push进数组里面，因为返回的都是当前的拦截器 pipe(3).double === pipe(3)
// 最后get的时候就是计算三个函数的值,每次拿上次返回的值做操作
// double = 3 * 2 = 6
// pow = 6 * 6 = 36
// reverseInt = 63

let count = pipe(3).double.pow.reverseInt.get; // 63
console.log(count)


/**
 * 4.
 * 利用get拦截，实现一个生成各种 DOM 节点的通用函数dom
 */

 const dom = new Proxy({}, {
   get (target, key) {
     return function (attr = {}, ...childrens) {
       const el = document.createElement(key);
       for (let prop of Object.keys(attr)) {
         el.setAttribute(prop, attr[prop])
       }

       for (let child of childrens) {
         if (typeof child === 'string') {
           child = document.createTextNode(child)
         }
         el.appendChild(child)
       }

       return el
     }
   }
 })

 const el = dom.div({},
  'Hello, my name is ',
  dom.a({href: '//example.com'}, 'Mark'),
  '. I like:',
  dom.ul({},
    dom.li({}, 'The web'),
    dom.li({}, 'Food'),
    dom.li({}, '…actually that\'s it')
  )
);

document.body.appendChild(el);


/**
 * 5.
 * get方法的第三个参数的例子，
 * 它总是指向原始的读操作所在的那个对象，一般情况下就是 Proxy 实例。
 * proxy对象的getReceiver属性是由proxy对象提供的，所以receiver指向proxy对象。
 */

 const proxy5 = new Proxy({}, {
   get: (target, key, receiver) => receiver
 })

//  console.log(proxy5.getReceiver === proxy5)

 /**
  * d对象本身没有a属性，所以读取d.a的时候，会去d的原型proxy对象找。
  * 这时，receiver就指向d，代表原始的读操作所在的那个对象。
  */
 const d = Object.create(proxy5);
// console.log(d.a === d) // true



/**
 * 6.
 * 如果一个属性不可配置（configurable）且不可写（writable），
 * 则 Proxy 不能修改该属性，否则通过 Proxy 对象访问该属性会报错。
 */

 const target6 = Object.defineProperties({}, {
   foo: {
     value: 123,
     writable: false,
     configurable: false
   }
 })

 const handler6 = {
   get: (target, key) => Reflect.get(target, key)
 }

 const proxy6 = new Proxy(target6, handler6)
 proxy6.foo // // TypeError: Invariant check failed
 console.log( proxy6.foo)

module.exports = {
  proxy,
  proxy2,
  proxy3
}