/**
 * set方法用来拦截某个属性的赋值操作，
 * 可以接受四个参数，
 * 依次为目标对象、属性名、属性值和 Proxy 实例本身，其中最后一个参数可选。
 */


 /**
  * 1.
  * 由于设置了存值函数set，任何不符合要求的age属性赋值，都会抛出一个错误，
  * 这是数据验证的一种实现方法。
  */

 let validator = {
   set (target, key, value) {
     console.log(target[key]) // undefied
     if (key === 'age') {
       if (!Number.isInteger(value)) {
         throw new TypeError('年龄必须为整数')
       }
       if (value > 200) {
         throw new RangeError('年龄不合法')
       }
     }

     // 对满足条件的age属性以及其他属性，直接保存
    //  target[key] = value
    return Reflect.set(target, key, value)
   }
 }

 let person = new Proxy({}, validator)
//  person.age = 100

//  console.log(person.age)

//  person.age = 'young' // 报错
// person.age = 300 // 报错


/**
 * 2.
 * 我们会在对象上面设置内部属性，属性名的第一个字符使用下划线开头，表示这些属性不应该被外部使用。
 * 结合get和set方法，就可以做到防止这些内部属性被外部读写。
 * 
 * 
 * 只要读写的属性名的第一个字符是下划线，一律抛错，从而达到禁止读写内部属性的目的
 */
function invariant (key, action) {
  if (key[0] === '_') {
    throw new Error(`Invalid attempt to ${action} private "${key}" property`)
  }
}
 const handler2 = {
   get (target, key) {
     invariant(key, 'get')
     return Reflect.get(target, key)
   },

   set (target, key, value) {
     invariant(key, 'set')
      return Reflect.set(target, key, value)
   }
 }

 let Proxy2 = new Proxy({}, handler2)

//  Proxy2.prop = 3
//  console.log(Proxy2.prop)



 /**
  * 3.
  * set方法的第四个参数receiver，指的是原始的操作行为所在的那个对象，一般情况下是proxy实例本身
  */

  const handler3 = {
    set (target, key, value, receiver) {
      // target[key] = receiver
      return Reflect.set(target, key, receiver) // 这是一个值啊
    }
  }

  const Proxy3 = new Proxy({}, handler3)

  // Proxy3.foo = 1
  // console.log(Proxy3.foo) // Proxy {foo: Proxy}
  // console.log(Proxy3.foo === Proxy3) // true


  /**
   * 4
   * 设置myObj.foo属性的值时，myObj并没有foo属性，
   * 因此引擎会到myObj的原型链去找foo属性。myObj的原型对象proxy是一个 Proxy 实例，设置它的foo属性会触发set方法。
   * 这时，第四个参数receiver就指向原始赋值行为所在的对象myObj
   */

  const myObj = {}
  Object.setPrototypeOf(myObj, Proxy3)
//   myObj.foo = 'bar';
//   console.log(myObj.foo) // {}
// console.log(myObj.foo === myObj )// true
// console.log(myObj.foo === Proxy3 )// false



/**
 * 5
 * 如果目标对象自身的某个属性，不可写且不可配置，那么set方法将不起作用
 */
const obj = {}
 Object.defineProperty(obj, 'foo', {
   value: 'bar',
   writable: false
 })

 const handler4 = {
   set (target, key, value, receiver) {
     // target[key] = value
     return Reflect.set(target, key, value, receiver)
   }
 }

 const Proxy4 = new Proxy(obj, handler4)
 Proxy4.foo = '1111'
 console.log(Proxy4.foo)

 module.exports = {}