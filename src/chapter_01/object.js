//1.对象的简写模式，key和value相同的时候可以简写只写一个值
var a = 10;
var obj = {a:a};
console.log(obj.a)//10

var a = 10;
var obj = {a};
console.log(obj.a)//10

//具体的应用
var foo = (function(){
  function show(){
    console.log("show")
  }
    function show1(){
    console.log("show1")
  }
    function show2(){
    console.log("show2")
  }
    function show3(){
    console.log("show3")
  }
  
  // return{show:show,show1,show1}
   return{show,show1}//简化
})()

foo.show()
foo.show1()

//方法的简写，原来的模式
var obj =  {
  show:function(){
    console.log("jjk")
  }
}
obj.show()

//简写模式
var obj =  {
  show(){
    console.log("jjk")
  }，
  show1(){
    console.log("jjk")
  }
}
obj.show()


//2.key为参数形式,通过中括号可以找到
//value可以为参数
var value = "jjk"
var obj =  {
  key:value
}
console.log(obj.key)//jjk

var value = "jjk"
var name  = "key"
var obj =  {
  name:value
}
console.log(obj['key'])//underfined
console.log(obj.key)//underfined
console.log(obj.name)//jjk
console.log(obj[key])//报错ReferenceError: key is not defined


var value = "jjk"
var name  = "key"
var obj =  {
  [name]:value
}
console.log(obj.key)//jjk


//object的静态方法
//查看object的方法直接console.log(window)找到object
//IS方法：作比较
var a = 1;
var b = 1;
console.log(a==b)//true
console.log(Object.is(a,b))//true

var c = NaN;
var d = NaN;
console.log(c == d)//false
console.log(Object.is(c,d))//true

var a = +0;
var b = -0;
console.log(a==b)//true
console.log(Object.is(a,b))//false

//freeze冻结类型
var obj ={
  name:"jjk"
}
console.log(obj.name)//jjk
obj.name = "dk"
console.log(obj.name)//dk

var obj ={
  name:"jjk"
}
console.log(obj.name)//jjk
Object.freeze(obj)//只能获取不能修给,类似常量的感觉
obj.name = "dk"
console.log(obj.name)//jjk