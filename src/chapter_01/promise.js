// function foo(time){
// 	var promise = new Promise(function(resolve,reject){
// 		setTimeout(function(){
// 			console.log(time+"秒后执行");
// 			// resolve();
// 			reject()
// 		},time*1000)
// 	})
// 	return promise;
// }

// //promise对象的方法：then()
// foo(1).then(function(){
// 	console.log("成功");
// },function(){
// 	console.log("失败");
// })



//1.解决回调金字塔的问题then方法
// foo(1)
// 	.then(function(){
// 		console.log("操作1");
// 		return foo(2)
// 	})
// 	.then(function(){
// 		console.log("操作2");
// 		return foo(2)
// 	})
// 	.then(function(){
// 		console.log("操作3");
// 	})






// //2.catch可以直接填写失败的信息，promise对象的方法：then()
// foo(1).catch(function(){
// 	console.log("失败")
// })


//3.all 方法，当有多个promise都返回解决状态的时候才触发成功
//其中有一个未解决就会执行马上执行失败
function foo1(time) {
	var promise = new Promise(function(resolve, reject) {
		setTimeout(function() {
			console.log(time + "秒后执行");
			// resolve();
			reject()
		}, time * 1000)
	})
	return promise;
}
function foo2(time) {
	var promise = new Promise(function(resolve, reject) {
		setTimeout(function() {
			console.log(time + "秒后执行");
			// resolve();
			reject()
		}, time * 1000)
	})
	return promise;
}
function foo3(time) {
	var promise = new Promise(function(resolve, reject) {
		setTimeout(function() {
			console.log(time + "秒后执行");
			resolve();
			// reject()
		}, time * 1000)
	})
	return promise;
}


// var p = Promise.all([foo1(1),foo2(2),foo3(3)]);
// p.then(function(){
// 	console.log("成功");
// }, function(){
// 	console.log("失败");
// })



//4.all()哪一个先执行就会先返回哪一个的状态
var p = Promise.race([foo1(1),foo2(2),foo3(3)]);
p.then(function(){
	console.log("成功");
}, function(){
	console.log("失败");
})