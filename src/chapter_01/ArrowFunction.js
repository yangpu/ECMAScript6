//箭头函数：对函数进行简化操作
var fun = function(x) {
	return x;
}
fun(3)
var fun = x => x
fun(3)


var fun = function(x, y) {
	return x + y;
}
fun(3, 3)
var fun = (x, y) => x + y
fun(3, 4)


var fun = (x, y) => x * x + y * y
fun(3, 4)

//演示器的问题
setTimeout(function() {
	console.log("jjk")
}, 1000)
setTimeout(() => {
	console.log("jjk1")
}, 1000)


oBtn.onclick = function() {
	setTimeout(function() {
		this.style.color = "red";
		this.style.background = "black";
	}.bind(this), 1000)

}

//不用担心this的指向问题，会根据外面的this，不会影响
oBtn.onclick = function() {
		setTimeout(() => {
			this.style.color = "red";
			this.style.background = "black";
		}, 1000)
	}

//不适合构造函数
//不可使用arguments
//不可以用在Generator函数中