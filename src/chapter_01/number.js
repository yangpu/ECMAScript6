// Number.isFinite是否为有限的数字，只判断数字，不会自动转换
Number.isFinite(24) //true
Number.isFinite("89") //false


Number.isNaN(25)
Number.isNaN("hell")
Number.isNaN(NaN) //true


isNaN("eeee") //转不了数字智能转换为NaN


// Number.parseInt("332323");

// //判断是否为整数9.0整数
// Number.isInteger(9.9) //false


// Number.EPSILON console.log(0.1 + 0.2)
// function show(num1, num2) {
// 	return Math.abs(num1 - num2) < Number.EPSILON
// }
// show(0.1 + 0.2, 0.3)



//Math新增方法
//开立方
// Math.pow(27,1/3)
// Math.cbrt(27)

// //平方和
// Math.sqrt(3*3+4*4);
// Math.hypot(3,4);








//指数运算符可以与等号结合，形成一个新的赋值运算符（**=）有bug
//
// 2 ** 2 // 4
// 2 ** 3 // 8
// let a = 2;
// a **= 2;
// // 等同于 a = a * a;

// let b = 3;
// b **= 3;
// 等同于 b = b * b * b;
// console.log(2 ** 2);