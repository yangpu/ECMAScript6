//面向对象
//1.构造函数
function Aaa(x, y) { //模拟出来的类
	this.x = x;
	this.y = y;
}
Aaa.prototype.sum = function() {
	return this.x + this.y;
}
Aaa.prototype.num = 6; //直接在原型下面添加属性


var a = new Aaa(1, 2);
console.log(a.sum()); //3
console.log(a.num); //6



//用es6的class类不能反复命名
class Go {
	constructor(x, y) {
			this.x = x;
			this.y = y;
		}
		//类似上面挂载到原型上面的
	sum() {
			return this.x + this.y;
		}
		//原型链下面的属性
	get number() {
		return 32;
	}
}
var a = new Go(3, 4);
console.log(a.sum()) // 7
console.log(a.sum == Go.prototype.sum) //true
console.log(a.number) //32
console.log(a.number == Go.prototype.number) //true
console.log(a.x == Go.prototype.x) //false


//3.es5的静态方法
function Aaa(x, y) { //模拟出来的类
	this.x = x;
	this.y = y;
}

Aaa.staticPro = "hello"; //直接在原型下面添加属性
Aaa.staticMethod = function() {
	return Aaa.staticPro + "es5"
}

var a = new Aaa(1, 2);

console.log(Aaa.staticPro); //hello不能用a调用否则报错
console.log(Aaa.staticMethod()) //helloes5

//es6
class Go {
	//静态方法和属性
	static get staticPro() {
		return "hello";
	}
	static staticMethod() {
		return Go.staticPro + "es6";
	}
}

var a = new Go(1, 2);

console.log(Go.staticPro); //hello不能用a调用否则报错
console.log(Go.staticMethod()) //helloes6



//3.es5存在变量提升，es6不存在变量提升
var a = new Aaa(1, 2);//存在变量提升
function Aaa(x, y) { //模拟出来的类
	this.x = x;
	this.y = y;
}
Aaa.prototype.sum = function() {
	return this.x + this.y;
}
console.log(a.sum()); //3必须放在后面否则报错

var a = new Go(3, 4);//报错ReferenceError: can't access lexical declaration `Go' before initialization
class Go {
	constructor(x, y) {
			this.x = x;
			this.y = y;
		}

	sum() {
			return this.x + this.y;
		}
}


console.log(a.sum()) // 7





//继承
//es5
function Aaa(x, y) { //父类
	this.x = x;
	this.y = y;
}
Aaa.prototype.sum = function() {
	return this.x + this.y;
}

function Bbb(x,y){//子类
	Aaa.call(this,x,y)//继承父类的属性
}
var F = function(){};
F.prototype = Aaa.prototype;
Bbb.prototype = new F();
Bbb.prototype.constructor = Bbb;

var b = new Bbb(3,4)
console.log(b.sum());//7


//es6
class Go {//父类
	constructor(x, y) {
			this.x = x;
			this.y = y;
		}

	sum() {
			return this.x + this.y;
		}
}
class Bbb extends Go{}//子类
var b = new Bbb(3,5);
console.log(b.sum());//8


class Go {//父类
	constructor(x, y) {
			this.x = x;
			this.y = y;
		}

	sum() {
			return this.x + this.y;
		}
}
var a = new Go(3,4)
console.log(a.sum())//7
console.log(a.x)//3
console.log(a.y)//4
class Bbb extends Go{
  constructor(){
    super()//一定要写在起始的位置
    this.z = 100
  }
}
var b = new Bbb(3,5);
console.log(b.z);//100
console.log(b.x);//underfined
console.log(b.y);//underfined






class Bbb extends Go{
  constructor(x,y){//一定要传参，否则为underfined
    super(x,y)//一定要传参，否则为underfined
    this.z = 100
  }
  sum(){//重写子类的方法
  	return 100;
  }
}
var b = new Bbb(3,5);
console.log(b.z);//100
console.log(b.sum())//100自动调用子类的



class Go {//父类
	constructor(x, y) {
			this.x = x;
			this.y = y;
		}

	sum() {
			return this.x + this.y;
		}
}
class Bbb extends Go{
  constructor(x,y){//一定要传参，否则为underfined
    super(x,y)//一定要传参，否则为underfined
    this.z = 100
    console.log(this.sum())//100调用子类的方法，没有就调父类
    console.log(super.sum)//8调用父类的方法
  }
  sum(){//重写子类的方法
  	return 100;
  }
}
var b = new Bbb(3,5);