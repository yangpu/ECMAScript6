function show() {
	Array.from(arguments).map(function(val) {
		console.log(val);
	})
}
show(1, 3, 4)

//  1
// 3
// 4



var a = [1, 2, 3]
var b = Array.from(a)
b.push(4)
console.log(b) // [1, 2, 3, 4]
console.log(a) //[1, 2, 3]

var a = [1, 2, 3]
var b = a
b.push(4)
console.log(b) // [1, 2, 3, 4]
console.log(a) // [1, 2, 3, 4]


Array.of(1, 3, 4) //[1, 3, 4]
var arr = new Array(3)
arr //[undefined, undefined, undefined]
Array.of(3) //[3]

//找到2的位置，然后从3开始到4结束放到2的位置上
var arr = [1, 2, 3, 4, 5]
arr.copyWithin(2, 3, 4)
	//[1, 2, 4, 4, 5]

//[4, 5, 3, 4, 5]
var arr = [1, 2, 3, 4, 5]
arr.copyWithin(0, 3)


var arr = [1, 2, 3, 4, 5]
var str = arr.find(function(val) {
	return val > 3;
})

console.log(str) //4


var arr = [1, 2, 3, 4, 5]
var str = arr.find(val => val > 3)
console.log(str)


var arr = [1, 2, 3, 4, 5]
arr.fill(7)
	//[7, 7, 7, 7, 7]全部填充7；


var arr = [1, 2, 3, 4, 5]

arr.fill(7, 2, 3)
	//[1, 2, 7, 4, 5]从2开始到3结束，填充7

var arr = [1, 2, 3, 4, 5]
arr.fill(7, 2)
	//[1, 2, 7, 7, 7]从2开始到结束填充7


var arr = [1, 2, 3, 4, 5]
arr.indexOf(8)
	//-1
arr.indexOf(3)
	//2


arr.includes(9)
	//FALSE