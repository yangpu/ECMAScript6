//es6提供的一种模式，就是对数组和对象进行赋值操作

var [a,b,c] = [1,2,3]
console.log(a);
console.log(b);
console.log(c);




//变量名一定要和key的值相同，对象没有顺序，只能这样；一定要两者匹配，是无序数组
var {myname,myage}={myname:"jjk",myage:16};
//是{myname：myname,myage：myage}的简写
console.log(myname);
console.log(myage);

var {name:a,age}={name:"jjk",age:16};
console.log(a);
console.log(age);

//都输出jjk     16

var {myname:a,myage:b}={myname:"jjk",myage:13};
// console.log(myname)报错
console.log(a)


let myna
({myna:a}= {myna:"jjk"});
console.log(a)




//对象一定要对应上,key一定要是a
var obj = [1,{a:7},2]
var [e,{a},c] = obj;
console.log(e);
console.log(a);
console.log(c)


var {myname:a,myage:b}={myname:"jjk",age:15,myage:13};
console.log(a)
console.log(b)


//字符串形式  TypeError: 123 is not iterable
//TypeError: true is not iterable
//布尔值，数字类型都不可以
//拥有iterable接口的都可以进行解构赋值
var [a,b] = "hello";
console.log(a)
console.log(b)
//he





//1.用于ajax
function ajax(options){
  var {url,type,data} = options;
  console.log(url)
  console.log(type)
  console.log(data)
//   www.baidu.com
// post
// [1, 2, 3]
}

function ajax1({url,type,data}){

  console.log(url)
  console.log(type)
  console.log(data)
}

ajax({url:"www.baidu.com",type:"post",data:[1,2,3]})
ajax1({url:"www.baidu.com",type:"post",data:[1,2,3]})






//2.数据互换
var [a,b] = [1,10];
console.log(a)
console.log(b)

var [a,b] = [b,a]
console.log(a)
console.log(b)


//3.默认参数
var [a=10,b=3] = [2,7];
console.log(a);
console.log(b);

var {x=3,y=4}={}
console.log(x);
console.log(y)

function show(x=19){
console.log(x)
}
show()


//ajax的互换值
function ajax1({url,type="get",data}){

  console.log(url)
  console.log(type)
  console.log(data)
}

ajax1({url:"www.baidu.com",data:[1,2,3]})



//rest参数：不定（剩余）参数返回数组（必须加到最后）
var [a,b,c] = [1,2,3,4,5];
console.log(a)
console.log(b)
console.log(c)//3

var [a,b,...c] = [1,2,3,4,5];
console.log(a)
console.log(b)
console.log(c)//[3, 4, 5]

function show(a,b,...c){
  console.log(c)
}
show(1,2,3,4,5,6)//[3, 4, 5, 6]



function show(a,b,c){
  console.log(show.length)//形参的个数3
}
show(1,2,3,4,5,6)

function show(a,b,...c){
  console.log(show.length)//函数的参数不包括reset参数2
}
show(1,2,3,4,5,6)

//拓展预算，reset的逆运算
var arr = [1,2,3,4,5]
console.log(...arr)

var arr = [1,3,4]
function show(a,b,c){
  console.log(`${a}是 ${b}是 ${c}`)
}
show(...arr)






