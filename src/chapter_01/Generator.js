// Generator生成器
// 是es6提供的一种异步编程的解决方案
// 
// promise的方法比较复杂的，主要解决回调金字塔的问题
// 
// 更适合解决异步编程
// 
// 
// 
// 写法
// 1.function*
function* foo(){
	var a = 10;
}


//yield
function* foo(){
	yield  10;
	yield  11;
	yield  12;
}
//foo();//babel转码会报错
//Generator {}原生没有问题，
//需要brower
//babel转码插件
var it =  foo();
console.log(it.next())// Object { value=10,  done=false}
console.log(it.next())//Object { value=11,  done=false}
console.log(it.next())//Object { value=12,  done=false}
console.log(it.next())//Object { done=true,  value=undefined}

for(var v of it){
	console.log(v)//10,11,12value值
}


//支持对象的遍历器接口
var obj = {
	name:"jjk",
	age:"34",
	*[Symbol.iterator](){
		for(var k in this){
			yield this[k];
		}
	}
}
for(var v of obj){
	console.log(v);
}


//next()参数
function* foo(){
	var a = yield 5;
	yield a+ 3;
}
var g = foo();
console.log(g.next())// Object { value=5,  done=false}
console.log(g.next())// Object { value=NaN,  done=false}，a不能识别
console.log(g.next(123));//123代表上一次yield的返回值    Object { value=126,  done=false}




// 异步操作的同步化表达
function foo(){
	var result = asyncMethod(5)//500ms之后才返回结果
	console.log(result);
}
function asyncMethod(x){
	setTimeout(function(){
		return x*x;
	},500)
}
foo();//undefined


// 用Gernerator解决
function* foo(){
	var result = yield asyncMethod(5);//第一个next
	console.log(result);//第二个next
}
function asyncMethod(x){
	setTimeout(function(){
		g.next(x*x);//第二个next
	},500)
}
var g = foo();//返回接口
g.next()