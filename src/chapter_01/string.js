//includes()
//返回布尔烈性，判断字符串里是否有该字符

var str = "hello"
str.indexOf("hp");

str.includes("hp")
str.startsWith("ho")
str.endsWith("o")



//0就是空的字符串
str.repeat(2)


var str = "hello"

//"xyzxyhello"一共为10个字符串，向前补5个，低于5个就输出原数
str.padStart(10,"xyz")
var str = "hello"  
str.padEnd(13,"xyz")



//正则
var str = "aaa_aa_a";
var re = /a+/igm;
str.match(re);
//["aaa", "aa", "a"]


var re = /a+/iygm;
//["aaa"]


re.sticky
re.flags

//前行断言
var str = "100%200px";
var re = /\d+%/g;
str.match(re)
//["100%"]


var str = "100%200px";
var re = /\d+(?=%)/g;
str.match(re)
//100