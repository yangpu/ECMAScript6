var obj = {
	age: 15
};

function show() {
	obj.age = 17;
	console.log(obj.age) //17
}
show()
console.log(obj.age) //17


//用来解决
var obj = {
	age: 15
};

function show() {
	var age = Symbol()
	obj[age] = 17;
	console.log(obj[age]) //17
}
show()
console.log(obj.age) //15co
console.log(obj[age]) //报错
console.log(obj['age'])//15
console.log(obj[Symbol()])//underfined


var sym = Symbol();
console.log(sym)// Symbol {}
console.log(sym+"ggggg")//报错，不能进行运算操作

var sym = Symbol();
var sym2 = Symbol();
console.log(sym == sym2)//不行false内存地址不同

var sym1 = Symbol("foo");
var sym2 = Symbol("foo");
console.log(sym1 == sym2)//false

var sym = Symbol();
var sym2 = Symbol();
console.log(sym.toString())//Symbol()
console.log(sym2.toString())//Symbol()

// 通过参数可以区分哪一个是哪一个symbol
var sym = Symbol('faf');
var sym2 = Symbol('fad');
console.log(sym.toString())//Symbol(faf)
console.log(sym2.toString())//Symbol(fad)




var sym1 = Symbol.for("foo");
var sym2 = Symbol.for("foo");
console.log(sym1 == sym2)//true引用相同

//提供了两个方法
//Symbol.for的应用
var obj = {}
function show(){
  var age = Symbol();
  obj[age] = 'jjk';
  console.log(obj[age])//jjk
}
show()

function hide(){
  console.log(obj[age])//报错
}
hide()

function hide(){
  var age = Symbol();
  console.log(obj[age])//underfined
}
hide()


//应用：都可以打印出来
var obj = {}
function show(){
  var age = Symbol.for('a');
  obj[age] = 'jjk';
  console.log(obj[age])//jjk
}
show()

function hide(){
  var age = Symbol.for('a');
   console.log(Symbol.keyFor(age))//找到a
  console.log(obj[age])//jjk
}
hide()
