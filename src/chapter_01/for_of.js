// for写法不够简洁
// forEach不能跳出循环操作，return
// forin 打印出key值，主要用来输出key值，索引为字符串，，



var arr = ['a', 'b', 'c']; //可拓展属性也会被遍历
arr.name = 'hello';
for (var v in arr) {
	console.log(v);
}
// 0
// 1
// 2
// name


var obj = {
	name； "jjk",
	11111: 22222,
	age: 13
}; //无序
for (var key in obj) {
	console.log(key);
}
// 1111
// name
// age


var arr = ['a', 'b', 'c']; //for of
arr.name = 'hello';
for (var v of arr) {
	console.log(v);
}
//a,b,c不会输出自带属性hello

for (var v of arr) {
	console.log(v);
	break;
}
//a


//用于字符串 h t l l o
var str = "htllo";
for (var v of str) {
	console.log(v)
}

//类数组
function foo() {
	for (var v of arguments) {
		console.log(v); //1,2,3,4,5,6
	}
}
foo(1, 2, 3, 4, 5, 6)

//对象
var obj = {
	name: "nc",
	age: 23
}; //TypeError: obj is not iterable
for (var v of obj) {
	console.log(v);
}

//Object.keys()
var obj = {
	name: "nc",
	age: 23
};
for (var v of Object.keys(obj)) {
	console.log(v); //name,age
}

//数组去重
var arr = []


/**
 * *******************************set*****************************************
 */
var s = new Set(['a', 'b', 'c']);
console.log(s.length); //underfined
console.log(s.size); //3
for (var v of s) {
	console.log(v); //a,b,c
}
for (var v of s.keys()) {
	console.log(v); //a,b,c
}
for (var v of s.values()) {
	console.log(v); //a,b,c
}
var s = new Set(['a', 'b', 'c']);
for (var v of s.entries()) {
	console.log(v);
	// ["a", "a"]
	// ["b", "b"]
	// ["c", "c"]
}

//可以进行foreach


//自带去重功能
var s = new Set(['a', 'b', 'c', 'b', 'c', 'b', 'c']);
console.log(s.size); //3



var arr = ['a', 'b', 'c', 'b', 'c', 'b', 'c'];
var newarr = [];
var obj = {};
for (var i = 0; i < arr.length; i++) {
	if (!obj[arr[i]]) {
		newarr.push(arr[i]);
		obj[arr[i]] = 1;
	}
};
console.log(newarr) //["a", "b", "c"]

//es6去重功能
var arr = ['a', 'b', 'c', 'b', 'c', 'b', 'c'];
var newarr = [...new Set(arr)]; //并不是数组
console.log(newarr); //abc

//或者用arrayfrom
var arr = ['a', 'b', 'c', 'b', 'c', 'b', 'c'];
var newarr = Array.from(new Set(arr));
console.log(newarr);



//1.相关属性
//add添加
var s = new Set();
s.add('a');
s.add("b");
console.log(s.size); //2;

//2.delete
s.delete('a');
console.log(s.size)

//3.has判断
console.log(s.has('a')); //false

//4.clear删除
s.clear();
console.log(s.size) //0



/**
 * *******************************map*****************************************
 * 
 */
var m = new Map([
	["name", "hello"],
	["age", "100"]
])
console.log(m.size); //2
console.log(m.length); //underfined;

for (var v of m) {
	console.log(v);
}
//  ["name", "hello"]
// ["age", "100"]

for (var v of m.keys()) {
	console.log(v); //name age
}
for (var v of m.values()) {
	console.log(v); //hello 100;
}
for (var v of m.entries()) {
	console.log(v);
}
//  ["name", "hello"]
// ["age", "100"]

//各种类型的简直都可以当value值，map数据结构可以用任何类型的key值来做key
//
//
//相关操作方法
//set()添加
var m = new Map([
	["name", "hello"],
	["age", "100"]
])
m.set("phone", "jjk")
console.log(m.size) //3



//get()获取
console.log(m.get("name")); //htllo
console.log(m.get("age")); //100
//相关遍历方法
//keys()
//values()
//entries()



/**
 * *******************************interator*****************************************
 * 
 */
//通过next的方法移动到下一个对象
var arr = ["a", "b", "c"];
var it = arr[Symbol.iterator]() //symbol,防止全局污染
console.log(it.next()) // Object { value="a",  done=false}
console.log(it.next()) // Object { value="b",  done=false}
console.log(it.next()) // Object { value="c",  done=false}
console.log(it.next()) // Object { value=underfined,  done=true}

var str = 'hello';
var it = str[Symbol.iterator](); //是一个function
console.log(it.next()) // Object { value="h",  done=false}


//对象加一个便利器借口
//
var obj = {
	name: "jjk",
	age: "15",
	[Symbol.iterator]() {
		var result = [];
		var count = 0;
		for (var v in this) {
			console.log(this.v);

		}
		return {

			next() {
				console.log(123)
			}
		}
	}
}
var it = obj[Symbol.iterator]();
it.next(); //123



var obj = {
	name: "hello",
	age: '18',
	[Symbol.iterator]() {
		var result = [];
		var count = 0;
		for (var k in this) {//this指代obj
			//字符串不能用点号
			result.push(this[k]);
		}
		return {
			next() {
				var obj = {};
				if (count < result.length) {
					obj = {
						value: result[count],
						done: false
					};
				} else {
					obj = {
						value: undefined,
						done: true
					};
				}
				count++;
				return obj;
			}
		}
	}
}
var it = obj[Symbol.iterator]();
console.log(it.next());
console.log(it.next());
console.log(it.next());
//  Object { value="hello",  done=false}
// Object { value="18",  done=false}
// Object { done=true,  value=undefined}
for(var k of obj){
	console.log(k);//拥有遍历器接口hello，16
}