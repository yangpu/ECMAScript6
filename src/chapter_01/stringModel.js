//字符串模板

//1.反引号

$(document).ready(function() {
	var str = `
	<div>
		jjk
	</div>
			`;
	$("li").eq(3).html(str);

	//2.横版站位符

	var str = "hello";
	console.log(`${str} jjk`);

	//3.支持简单的运算
	var str = `${1+1}`;
	console.log(str);

	var str = `${true?1:2}`;
	console.log(str);


	//4.嵌套和循环输出
	var list = [{text:111111},{text:111111},{text:111111}];
	$("div:first").html(`
		<ul>
			${list.map(function(obj) {
				return `<li>${obj.text}</li>`;
			}).join("")}
		</ul>
		`);


	var list = [1,2,3];

	//5.例子
	var str = `
	<ul>
	${list.map(function(val){
	  return `<li>${val}</li>`;
	}).join(``)}
	</ul>`

	$("div:last").html(str);

})