	var oDov1 = document.getElementById("div1")
	var oDov2 = document.getElementById("div2")
	console.log(oDov1);

	class Drag { //父类拖拽
		constructor(elem) {
			this.elem = elem;
			this.disX = 0;
			this.disY = 0;
		}
		drag() {
			this.elem.onmousedown = (ev) => {
				this.down(ev)
			};
		}
		down(ev) {
			this.disX = ev.pageX - this.elem.offsetLeft;
			this.disY = ev.pageY - this.elem.offsetTop;

			document.onmousemove = (ev) => {
				this.move(ev);
			}
			document.onmouseup = (ev) => {
				this.up()
			}
		}
		move(ev) {
			this.elem.style.left = ev.pageX - this.disX + "px";
			this.elem.style.top = ev.pageY - this.disY + "px";
		}
		up() {
			document.onmousemove = null;
			document.onmouseup = null;
		}
	}

	var a = new Drag(oDov1);
	a.drag();