//let属性
//1.块级作用域

// if(true){
// 	var a = 11;
// }
// console.log(a);
// 

// if(true){
// 	let a = 11;
// }
// //underfined 直接报错
// console.log(a)

window.onload = function() {
	var oLi = document.getElementsByTagName('li');
	for (let i = 0; i < oLi.length; i++) {
		oLi[i].onclick = function() {
			console.log(i);
		}
	}

	// for (var i = 0; i < oLi.length; i++) {
	// 	//闭包1
	// 	 (function(i) {
	// 		oLi[i].onclick =function(){
	// 			console.log(i);
	// 		}
	// 	})(i);
	// }
}


// //2.不能重复定义
// var a = 10;
// //直接报错
// let a = 23; 


//3.全局不能成为window的属性








